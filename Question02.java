import java.util.Scanner;

public class Question02 {

	public static void main(String[] args) {
		
		System.out.println("Enter no of lines(odd No):");
		try (Scanner sc = new Scanner(System.in)) {
			int n=sc.nextInt();
		

			
			for(int i=1; i<=n; i++) {
				
				for(int j=n-1; j>=i; j--)
					System.out.print(" ");
				
				for(int jp=1; jp<=i; jp++)
					System.out.print("/");
				
				
				for (int j = 1; j <= i; j++) {
					System.out.print("\\");
				}
				System.out.println();
				
			}

		
			for (int i = 0; i <= n; i++) {
				
				for (int j = 1; j <= i; j++) {
					
					System.out.print(" ");
		
				}
				for (int s = 0; s <=(n-1)-i; s++) {
					System.out.print("\\");
				}
				for (int s = n-1; s >= i; s--)
					System.out.print("/");
				
				System.out.println();
			}
		}
	}
	
}
